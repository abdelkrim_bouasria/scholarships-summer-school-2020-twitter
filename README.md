-   [1 Environment setup](#environment-setup)
-   [2 Load tweets data](#load-tweets-data)
-   [3 Show the popular tweets](#show-the-popular-tweets)
    -   [3.1 prepare data](#prepare-data)
    -   [3.2 Select the most popular
        tweets](#select-the-most-popular-tweets)
    -   [3.3 Show the most popular
        tweets](#show-the-most-popular-tweets)
-   [4 Tweeting activity analysis](#tweeting-activity-analysis)
    -   [4.1 Temporal trends](#temporal-trends)
        -   [Temporal trends for all
            tweets](#temporal-trends-for-all-tweets)
        -   [Temporal trends by type of
            tweets](#temporal-trends-by-type-of-tweets)
        -   [Temporal trends for each lecturer
            tweets](#temporal-trends-for-each-lecturer-tweets)
        -   [Temporal trends for each lecturer by type of
            tweets](#temporal-trends-for-each-lecturer-by-type-of-tweets)
    -   [4.2 Frequency of Tweet types](#frequency-of-tweet-types)
        -   [Frequency of Tweets type for all
            lecturers](#frequency-of-tweets-type-for-all-lecturers)
        -   [Frequency of Tweets type for each
            lecturer](#frequency-of-tweets-type-for-each-lecturer)
-   [5 Type of tweets and devices (frequency and
    trend)](#type-of-tweets-and-devices-frequency-and-trend)
-   [6 Most frequent words](#most-frequent-words)
    -   [6.1 Build keywords tidy table](#build-keywords-tidy-table)
    -   [6.2 Stop words building and cleaning
        data](#stop-words-building-and-cleaning-data)
        -   [Rtweet stop words](#rtweet-stop-words)
        -   [Build list of stop words of mentioned users in
            tweets](#build-list-of-stop-words-of-mentioned-users-in-tweets)
        -   [Build list of stop words of mentioned locations in
            tweets](#build-list-of-stop-words-of-mentioned-locations-in-tweets)
        -   [List of stop words of german
            language](#list-of-stop-words-of-german-language)
        -   [List of custom stop words](#list-of-custom-stop-words)
        -   [Most frequent 100 words to
            validate](#most-frequent-100-words-to-validate)
    -   [6.3 Validate keywords](#validate-keywords)
        -   [Keywords temporal trends](#keywords-temporal-trends)
        -   [Keywords spatial
            distribution](#keywords-spatial-distribution)
-   [7 Sentiment Analysis](#sentiment-analysis)
    -   [7.1 Data preparation and cleaning with
        tidytext](#data-preparation-and-cleaning-with-tidytext)
    -   [7.2 Sentiment matching following NRC
        standard](#sentiment-matching-following-nrc-standard)
        -   [Teen most frequent
            sentiments](#teen-most-frequent-sentiments)
        -   [Total sentiment frequence by
            lecturer](#total-sentiment-frequence-by-lecturer)
        -   [Frenquence ot sentiment
            type](#frenquence-ot-sentiment-type)
        -   [Frenquence ot sentiment type by
            lecturer](#frenquence-ot-sentiment-type-by-lecturer)
        -   [Frenquence ot sentiment type by tweet
            type](#frenquence-ot-sentiment-type-by-tweet-type)
    -   [7.3 Temporal trend sentiment](#temporal-trend-sentiment)
        -   [Overall trends](#overall-trends)
        -   [Sentiment analysis full](#sentiment-analysis-full)
        -   [Sentiment analysis (positive /
            negative)](#sentiment-analysis-positive-negative)
        -   [Comparison Word Cloud of positive and negative
            words](#comparison-word-cloud-of-positive-and-negative-words)
-   [8 Spatial distribution](#spatial-distribution)
-   [9 Social Network Analysis](#social-network-analysis)
    -   [9.1 show the most frequently used
        hashtags](#show-the-most-frequently-used-hashtags)
    -   [9.2 accounts from which most
        mentioned](#accounts-from-which-most-mentioned)

1 Environment setup
===================

Google slides: [Twitter analysis](https://docs.google.com/presentation/d/e/2PACX-1vTPgoU2qxAPsDGY6w02Phib_RyJSvqO6K7YcvHvPQAOoPU-nDZSBYEfEJj0plbxMXjwVRNi96OdR1vK/pub?start=false&loop=false&delayms=3000)

Video presentation: [Twitter analysis](https://www.youtube.com/watch?v=fy1teuWC3Mo&list=PLXUoTpMa_9s0Ea--KTV1OEvgxg-AMEOGv&index=41)



2 Load tweets data
==================

    # load tweets data
    tweets_tidy <- readRDS("data/tweets_tidy.rds")

3 Show the popular tweets
=========================

3.1 prepare data
----------------

    tweets_tidy <- tweets_tidy %>% mutate_at(c(1:3,5:16), ~replace(., is.na(.), 0))%>%
      mutate(rank_count = favorite_count+ retweet_count+quote_count+reply_count)

3.2 Select the most popular tweets
----------------------------------

Select tweets basing on 3 type of popularity:

1.  the most favorite,
2.  the most retweeted, and
3.  which meet both criteria

<!-- -->

    most_frequent_tweets <- list(
    # most favorite
      favorite = tweets_tidy %>% 
        filter(tweet_type == "organic")%>%
        arrange(-favorite_count) %>% head(1),

    # most retweeted
    retweeted = tweets_tidy %>% 
        filter(tweet_type == "organic")%>%
      arrange(-retweet_count) %>% head(1),

    # most ranked
    ranked = tweets_tidy %>%
      filter(tweet_type == "organic")%>%
      arrange(-rank_count) %>% head(1)
    ) %>% 
      # merge results
      bind_rows(.id = "popularity_type")%>%
      select(tweet_type, screen_name, name,status_id,
             text, created_at, favorite_count, 
             retweet_count,rank_count)

    most_frequent_tweets

    ## # A tibble: 3 x 9
    ##   tweet_type screen_name name  status_id text  created_at         
    ##   <chr>      <chr>       <chr> <chr>     <chr> <dttm>             
    ## 1 organic    jakub_nowo~ Jaku~ 12674584~ "At ~ 2020-06-01 14:10:15
    ## 2 organic    robinlovel~ Robi~ 10467295~ "Jus~ 2018-10-01 11:52:33
    ## 3 organic    geospacedm~ Barr~ 89131255~ "Sta~ 2017-07-29 15:00:47
    ## # ... with 3 more variables: favorite_count <dbl>, retweet_count <dbl>,
    ## #   rank_count <dbl>

3.3 Show the most popular tweets
--------------------------------

    tweets_screenshot <- NULL
    for(i in 1:nrow(most_frequent_tweets)){
      tweets_screenshot[i] <- print(tweet_screenshot(tweet_url(most_frequent_tweets$screen_name[i], most_frequent_tweets$status_id[i])))
    }

    screenshots_folder <- "screenshots/"
    file.copy(tweets_screenshot, screenshots_folder)

    ## [1] TRUE TRUE TRUE

    tweets_name <- data.frame(oldname = str_extract(tweets_screenshot,"file\\w+.png"),
                              newname = c("01fav.png","02retw.png","03rank.png"))
     #file.remove(paste("screenshots/",list.files("screenshots/",pattern ="^file\\w+.png"),sep=""))
     
     file.rename(from = file.path(screenshots_folder, tweets_name$oldname), to = file.path(screenshots_folder, tweets_name$newname))

    ## [1] TRUE TRUE TRUE

    tweets_screenshot <- list.files(screenshots_folder, ".png$")

-   *The most favorite tweet:* ![The most favorite
    tweet](screenshots/01fav.png)

-   *The most retweeted:* ![The most retweeted](screenshots/02retw.png)

-   *The most ranked tweet:* **favorite + retweeted** ![The most ranked
    tweet](screenshots/03rank.png)

4 Tweeting activity analysis
============================

The temporal trends are plotted by week, month, and then by year

4.1 Temporal trends
-------------------

### Temporal trends for all tweets

    # By week
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      ts_plot("weeks") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / Week")

![](README_files/figure-markdown_strict/unnamed-chunk-6-1.png)

    # By month
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      ts_plot("months") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / month")

![](README_files/figure-markdown_strict/unnamed-chunk-6-2.png)

    # By year
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      ts_plot("years") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / year")

![](README_files/figure-markdown_strict/unnamed-chunk-6-3.png)

### Temporal trends by type of tweets

    # By week
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(tweet_type)%>%
      ts_plot("weeks") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / Week / type of tweets")

![](README_files/figure-markdown_strict/unnamed-chunk-7-1.png)

    # By month
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(tweet_type)%>%
      ts_plot("months") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / month / type of tweets")

![](README_files/figure-markdown_strict/unnamed-chunk-7-2.png)

    # By year
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(tweet_type)%>%
      ts_plot("years") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / year / type of tweets")

![](README_files/figure-markdown_strict/unnamed-chunk-7-3.png)

### Temporal trends for each lecturer tweets

    # By week
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(name)%>%
      ts_plot("weeks") +
      facet_wrap( ~ name, scales = "free_y") +
      theme_minimal() +
      theme(legend.position = "none")+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / Week")

![](README_files/figure-markdown_strict/unnamed-chunk-8-1.png)

    # By month
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(name)%>%
      ts_plot("months") +
      facet_wrap( ~ name, scales = "free_y") +
      theme_minimal() +
      theme(legend.position = "none")+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / month")

![](README_files/figure-markdown_strict/unnamed-chunk-8-2.png)

    # By year
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(name)%>%
      ts_plot("years") +
      facet_wrap( ~ name, scales = "free_y") +
      theme_minimal() +
      theme(legend.position = "none")+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / year")

![](README_files/figure-markdown_strict/unnamed-chunk-8-3.png)

### Temporal trends for each lecturer by type of tweets

    # By week
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(name,tweet_type)%>%
      ts_plot("weeks") +
      facet_wrap( ~ name, scales = "free_y") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / Week")+
        theme(legend.position="bottom")

![](README_files/figure-markdown_strict/temp-trend-by-lect-1.png)

    # By month
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(name,tweet_type)%>%
      ts_plot("months") +
      facet_wrap( ~ name, scales = "free_y") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / month")+
        theme(legend.position="bottom")

![](README_files/figure-markdown_strict/temp-trend-by-lect-2.png)

    # By year
    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(name,tweet_type)%>%
      ts_plot("years") +
      facet_wrap( ~ name, scales = "free_y") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / year")+
        theme(legend.position="bottom")

![](README_files/figure-markdown_strict/temp-trend-by-lect-3.png)

4.2 Frequency of Tweet types
----------------------------

### Frequency of Tweets type for all lecturers

    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(tweet_type) %>%
      ggplot(aes(area=n, fill=tweet_type,label = tweet_type)) +
      geom_treemap() +
      geom_treemap_text(colour = "white", place = "centre")+
      theme_void() +
      #scale_fill_brewer(palette="Blues")+
      theme(legend.position = "none")+
        labs( title = "Frequency of Tweets type")

![](README_files/figure-markdown_strict/unnamed-chunk-9-1.png)

### Frequency of Tweets type for each lecturer

    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(name,tweet_type) %>%
      ggplot(aes(area=n, fill=tweet_type)) +
      geom_treemap() +
      theme_void() +
      facet_wrap(~name)+
      theme(legend.position = "right")+
        labs( title = "Frequency of Tweets type by lecturer")

![](README_files/figure-markdown_strict/unnamed-chunk-10-1.png)

5 Type of tweets and devices (frequency and trend)
==================================================

    tweets_tidy %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(source) %>%
      ggplot(aes(area=n, fill=source,label = source)) +
      geom_treemap() +
      geom_treemap_text(colour = "white", place = "centre")+
      theme_void() +
      theme(legend.position = "none")+
        labs( title = "Frequency of Tweets type")

![](README_files/figure-markdown_strict/unnamed-chunk-11-1.png)

6 Most frequent words
=====================

show the most frequent words found in the tweets

6.1 Build keywords tidy table
-----------------------------

    tweets_tidy_clean <- tweets_tidy
    tweets_tidy_clean$text = gsub(":", "", tweets_tidy_clean$text) # remove punctuation
    tweets_tidy_clean$text = gsub("/", "", tweets_tidy_clean$text) # remove punctuation
    tweets_tidy_clean$text = gsub("http\\w+", "", tweets_tidy_clean$text)  # remove links http
    tweets_tidy_clean$text = gsub(".co\\w+", "", tweets_tidy_clean$text)  # remove links http

    # remove punctuation, convert to lowercase, add id for each tweet!
    tweets_tidy_clean <- tweets_tidy_clean %>%
      select(tweet_type,status_id,text,created_at,user_id,screen_name,name,lat,long)%>%
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      unnest_tokens(word, text)

    nrow(tweets_tidy_clean)

    ## [1] 1466716

    # plot the top 50 words 
    tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word = reorder(word, n)) %>%
      ggplot(aes(x = word, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "keywords",
           title = "Most frequent 50 keywords\nbefore stop words removing")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-12-1.png)

6.2 Stop words building and cleaning data
-----------------------------------------

### Rtweet stop words

    stop_words <- select(stopwordslangs,word)
    tweets_tidy_clean <- tweets_tidy_clean %>%
      anti_join(stop_words)

    ## Joining, by = "word"

    nrow(tweets_tidy_clean)

    ## [1] 254754

    # plot the top 50 words 
    tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word = reorder(word, n)) %>%
      ggplot(aes(x = word, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "keywords",
           title = "Most frequent 50 keywords\nafter rtweet stop words removing")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-13-1.png)

### Build list of stop words of mentioned users in tweets

    tweets_full <- readRDS("data/tweets_full.rds")

    stop_words <-  dfm(tweets_full$text, remove_punct = TRUE)
    stop_words <- dfm_select(stop_words, pattern = "@*")
    stop_words <- data.frame(word = colnames(stop_words)) %>%
      mutate(word = as.character(word)) %>% unnest_tokens(word,word)
    stop_words <- rbind(stop_words,
                              tweets_tidy %>% select(screen_name) %>% rename(word = screen_name),
                              tweets_full %>% select(mentions_screen_name) %>% rename(word = mentions_screen_name)%>%
                                mutate(word=as.character(word)) %>% unnest_tokens(word,word) %>% filter(word!="c")
    ) %>% filter(!is.na(word)) %>% mutate(word = str_to_lower(word)) %>% distinct(word)
    rm(tweets_full)

    stop_words_anti_users <- read_csv("stop_words/anti_users.csv")

    ## Parsed with column specification:
    ## cols(
    ##   word = col_character()
    ## )

    stop_words <- stop_words %>% anti_join(stop_words_anti_users)

    ## Joining, by = "word"

    rm(stop_words_anti_users)

    tweets_tidy_clean <- tweets_tidy_clean %>%
      anti_join(stop_words)

    ## Joining, by = "word"

    nrow(tweets_tidy_clean)

    ## [1] 153487

    # plot the top 50 words 
    tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word = reorder(word, n)) %>%
      ggplot(aes(x = word, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "keywords",
           title = "Most frequent 50 keywords\nafter mentioned users removing")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-14-1.png)

### Build list of stop words of mentioned locations in tweets

    stop_words <- tweets_tidy %>% select(location)%>%
      rename(word=location) %>% unnest_tokens(word,word)%>%
      filter(!is.na(word) & word != "") %>% mutate(word = str_to_lower(word)) %>% distinct(word)

    stop_words_anti_users <- read_csv("stop_words/anti_users.csv")

    ## Parsed with column specification:
    ## cols(
    ##   word = col_character()
    ## )

    stop_words <- stop_words %>% anti_join(stop_words_anti_users)

    ## Joining, by = "word"

    rm(stop_words_anti_users)

    tweets_tidy_clean <- tweets_tidy_clean %>%
      anti_join(stop_words)

    ## Joining, by = "word"

    nrow(tweets_tidy_clean)

    ## [1] 150644

    # plot the top 50 words 
    tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word = reorder(word, n)) %>%
      ggplot(aes(x = word, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "keywords",
           title = "Most frequent 50 keywords\nafter mentioned locations removing")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-15-1.png)

### List of stop words of german language

    stop_words <- read_csv("stop_words/de_full.txt")

    ## Parsed with column specification:
    ## cols(
    ##   word = col_character()
    ## )

    tweets_tidy_clean <- tweets_tidy_clean %>%
      anti_join(stop_words)

    ## Joining, by = "word"

    nrow(tweets_tidy_clean)

    ## [1] 148372

    # plot the top 50 words 
    tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word = reorder(word, n)) %>%
      ggplot(aes(x = word, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "keywords",
           title = "Most frequent 50 keywords\nafter german stop words removing")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-16-1.png)

### List of custom stop words

    stop_words <- read_csv("stop_words/me.txt")

    ## Parsed with column specification:
    ## cols(
    ##   word = col_character()
    ## )

    tweets_tidy_clean <- tweets_tidy_clean %>%
      anti_join(stop_words)

    ## Joining, by = "word"

    nrow(tweets_tidy_clean)

    ## [1] 146825

    # plot the top 50 words
    tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word = reorder(word, n)) %>%
      ggplot(aes(x = word, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "keywords",
           title = "Most frequent 50 keywords\nafter custom stop words removing")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-17-1.png)

    rm(stop_words)

### Most frequent 100 words to validate

    most_frequent_100_words <- tweets_tidy_clean %>%
      count(word, sort = TRUE) %>%
      top_n(100) %>%
      mutate(word = reorder(word, n))

    ## Selecting by n

    most_frequent_100_words

    ## # A tibble: 100 x 2
    ##    word             n
    ##    <fct>        <int>
    ##  1 rstats        5728
    ##  2 qgis          3318
    ##  3 spatial       1815
    ##  4 rspatial      1268
    ##  5 raster         929
    ##  6 foss4g         719
    ##  7 geospatial     611
    ##  8 gdal           545
    ##  9 mapview        503
    ## 10 reproducible   408
    ## # ... with 90 more rows

    write_csv(most_frequent_100_words,"frequent_words/most_frequent_100_words.csv")
    rm(most_frequent_100_words)

6.3 Validate keywords
---------------------

    remove_words <- c("output", "repo", "extent", "handling",
                      "_dav", "exploring", "für", "rstudi",
                      "romain_fra", "developed", "solutions", 
                      "arcgis", "existing", "formats", "labels",
                      "manifold", "metadata", "preprint")

    for(i in 1: length(remove_words)){
      tweets_tidy_clean <- tweets_tidy_clean %>% filter(word != remove_words[i])
      }

    tweets_tidy_clean <-  tweets_tidy_clean %>%  
        mutate(word2 = ifelse(str_detect(word, "^ggplot$"), "ggplot2", word))%>% 
        mutate(word2 = ifelse(str_detect(word2, "^docs$"), "documentation", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "^foss4g2019$"), "foss4g", word2))%>% 
        mutate(word2 = ifelse(str_starts(word2, "^reproducib"), "reproducible", word2))%>%  
        mutate(word2 = ifelse(str_detect(word2, "^datasets$"), "dataset", word2)) %>%  
        mutate(word2 = ifelse(str_starts(word2, "^geocomp"), "geocomp", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "statistical"), "statistics", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "movementdataanalysis"), "movementdata", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "movingpandas"), "movementdata", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "polygons"), "polygons", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "spatio"), "spatiotemporal", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "rasters"), "raster", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "_sf$"), "sf", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "^sf"), "sf", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "^st_"), "sf", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "_sfc$"), "sf", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "geospatial$"), "geospatial", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "presenting"), "present", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "speakers"), "present", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "opendatahack"), "opendata", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "projections"), "proj", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "sentinel2"), "remotesensing", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "imagery"), "remotesensing", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "landsat"), "remotesensing", word2))%>% 
        mutate(word2 = ifelse(str_detect(word2, "^sensing$"), "remotesensing", word2))

    tweets_tidy_clean %>%
      count(word2, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word2 = reorder(word2, n)) %>%
      ggplot(aes(x = word2, y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "Keywords",
           title = "Final list of the most 50 frequent\nkeywords")

    ## Selecting by n

![](README_files/figure-markdown_strict/unnamed-chunk-20-1.png)

    most_frequent_50_words <- tweets_tidy_clean %>%
      count(word2, sort = TRUE) %>%
      top_n(50) %>%
      mutate(word2 = reorder(word2, n))

    ## Selecting by n

    write_csv(most_frequent_50_words,"frequent_words/most_frequent_50_words.csv")

    most_frequent_100_clean_words <- tweets_tidy_clean %>%
      count(word2, sort = TRUE) %>%
      top_n(100) %>%
      mutate(word2 = reorder(word2, n))

    ## Selecting by n

    write_csv(most_frequent_100_clean_words,"frequent_words/most_frequent_100_clean_words.csv")

    most_frequent_50_words$validate <- "ok"
    most_frequent_50_words$n <- NULL
    tweets_tidy_clean <- tweets_tidy_clean %>% inner_join(most_frequent_50_words)

    ## Joining, by = "word2"

    saveRDS(tweets_tidy_clean,"data/tweets_tidy_clean_50_wodrs.rds")
    rm(most_frequent_50_words)

### Keywords temporal trends

    # By week
    tweets_tidy_clean %>% 
      filter(validate =="ok")%>%
      group_by(word2)%>%
      ts_plot("weeks") +
      facet_wrap( ~ word2, scales = "free_y", ncol = 6) +
      theme_minimal() +
      theme(legend.position = "none")+
      labs(x = NULL, y = NULL,
           title = "Trend for each keyword / Week")

![](README_files/figure-markdown_strict/unnamed-chunk-22-1.png)

    # By month
    tweets_tidy_clean %>% 
      filter(validate =="ok")%>%
      group_by(word2)%>%
      ts_plot("months") +
      facet_wrap( ~ word2, scales = "free_y", ncol = 6) +
      theme_minimal() +
      theme(legend.position = "none")+
      labs(x = NULL, y = NULL,
           title = "Trend for each keyword / month")

![](README_files/figure-markdown_strict/unnamed-chunk-22-2.png)

    # By year
    tweets_tidy_clean %>% 
      filter(validate =="ok")%>%
      group_by(word2)%>%
      ts_plot("years") +
      facet_wrap( ~ word2, scales = "free_y", ncol = 6) +
      theme_minimal() +
      theme(legend.position = "none")+
      labs(x = NULL, y = NULL,
           title = "Trend for each keyword / year")

![](README_files/figure-markdown_strict/unnamed-chunk-22-3.png)

### Keywords spatial distribution

    World <- map_data("world")
    # global
    ggplot() +
      geom_polygon(data = World,aes(x = long, y = lat, group = group), fill = "grey82", color = "white",alpha = 0.6)+
      theme_void() +
      coord_quickmap() +
      geom_point(data = tweets_tidy_clean,
                 aes(x = long, y = lat),size = 2,alpha = 0.6) +
      labs(title = "The most 50 frequent keywords world map") +
      theme_void() +
      theme(legend.position="none")

    ## Warning: Removed 26367 rows containing missing values (geom_point).

![](README_files/figure-markdown_strict/unnamed-chunk-23-1.png)

    rm(World)

7 Sentiment Analysis
====================

7.1 Data preparation and cleaning with tidytext
-----------------------------------------------

    tweets_sa <- tweets_tidy %>%
      mutate(created_date = as.Date(created_at)) %>%
      unnest_tokens(word, text)
    tweets_sa %>% head()

    ## # A tibble: 6 x 20
    ##   tweet_type status_id created_at          source favorite_count retweet_count
    ##   <chr>      <chr>     <dttm>              <chr>           <dbl>         <dbl>
    ## 1 organic    12851500~ 2020-07-20 09:50:33 Twitt~            104            29
    ## 2 organic    12851500~ 2020-07-20 09:50:33 Twitt~            104            29
    ## 3 organic    12851500~ 2020-07-20 09:50:33 Twitt~            104            29
    ## 4 organic    12851500~ 2020-07-20 09:50:33 Twitt~            104            29
    ## 5 organic    12851500~ 2020-07-20 09:50:33 Twitt~            104            29
    ## 6 organic    12851500~ 2020-07-20 09:50:33 Twitt~            104            29
    ## # ... with 14 more variables: user_id <chr>, screen_name <chr>, name <chr>,
    ## #   followers_count <dbl>, friends_count <dbl>, statuses_count <dbl>,
    ## #   location <chr>, quote_count <dbl>, reply_count <dbl>, lat <dbl>,
    ## #   long <dbl>, rank_count <dbl>, created_date <date>, word <chr>

7.2 Sentiment matching following NRC standard
---------------------------------------------

    tweets_sa <- tweets_sa %>%
      inner_join(get_sentiments("nrc"), by = "word")
    tweets_sa %>% select(word, sentiment) %>% head()

    ## # A tibble: 6 x 2
    ##   word    sentiment
    ##   <chr>   <chr>    
    ## 1 library positive 
    ## 2 fusion  positive 
    ## 3 library positive 
    ## 4 hate    anger    
    ## 5 hate    disgust  
    ## 6 hate    fear

### Teen most frequent sentiments

    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(screen_name,name,sentiment) %>%
      top_n(10)%>%
      arrange(desc(n))

    ## Selecting by n

    ## # A tibble: 10 x 4
    ##    screen_name  name             sentiment        n
    ##    <chr>        <chr>            <chr>        <int>
    ##  1 mdsumner     Michael Sumner   positive     13846
    ##  2 geospacedman Barry Rowlingson positive     13458
    ##  3 geospacedman Barry Rowlingson negative      8626
    ##  4 geospacedman Barry Rowlingson trust         7937
    ##  5 mdsumner     Michael Sumner   trust         7583
    ##  6 geospacedman Barry Rowlingson anticipation  7249
    ##  7 mdsumner     Michael Sumner   negative      7196
    ##  8 mdsumner     Michael Sumner   anticipation  6624
    ##  9 underdarkGIS Anita Graser     positive      6367
    ## 10 mdsumner     Michael Sumner   joy           5655

### Total sentiment frequence by lecturer

    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(screen_name,name) %>%
      arrange(n)

    ## # A tibble: 30 x 3
    ##    screen_name     name                    n
    ##    <chr>           <chr>               <int>
    ##  1 ChKnoth         Christian Knoth         1
    ##  2 ben_graeler     Ben Gräler             51
    ##  3 christophfrdrch Christoph Friedrich    80
    ##  4 appelmar        Marius Appel          114
    ##  5 hanna123987     Hanna Meyer           204
    ##  6 jdsylvain       jean-daniel Sylvain   242
    ##  7 xurxosanz       Jorge                 323
    ##  8 creuden         Chris Reudenbach      339
    ##  9 menglugeo       Meng Lu               395
    ## 10 man_matej       Matej Man            1013
    ## # ... with 20 more rows

### Frenquence ot sentiment type

    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(sentiment) %>%
      ggplot(aes(area=n, fill=sentiment,label = sentiment)) +
      geom_treemap() +
      geom_treemap_text(colour = "white", place = "centre")+
      theme_void() +
      #scale_fill_brewer(palette="Blues")+
      theme(legend.position = "none")+
      labs( title = "Frequency of sentiment in Tweets")

![](README_files/figure-markdown_strict/unnamed-chunk-28-1.png)

### Frenquence ot sentiment type by lecturer

    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(name,sentiment) %>%
      ggplot(aes(area=n, fill=sentiment)) +
      geom_treemap() +
      theme_void() +
      facet_wrap(~name)+
      #scale_fill_brewer(palette="Blues")+
      theme(legend.position = "right")+
      labs( title = "Frequency of sentiment in Tweets by lecturer")

![](README_files/figure-markdown_strict/unnamed-chunk-29-1.png)

### Frenquence ot sentiment type by tweet type

    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      count(tweet_type,sentiment) %>%
      ggplot(aes(area=n, fill=sentiment)) +
      geom_treemap() +
      theme_void() +
      facet_wrap(~tweet_type)+
      #scale_fill_brewer(palette="Blues")+
      theme(legend.position = "right")+
      labs( title = "Frequency of sentiment by tweet type")

![](README_files/figure-markdown_strict/unnamed-chunk-30-1.png)

    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      filter(sentiment =="positive" | sentiment =="negative")%>%
      count(tweet_type,sentiment) %>%
      ggplot(aes(area=n, fill=sentiment)) +
      geom_treemap() +
      theme_void() +
      facet_wrap(~tweet_type)+
      #scale_fill_brewer(palette="Blues")+
      theme(legend.position = "right")+
      labs( title = "Frequency of sentiment by tweet type (positive / negative)")

![](README_files/figure-markdown_strict/unnamed-chunk-30-2.png)

7.3 Temporal trend sentiment
----------------------------

### Overall trends

    ggplot(tweets_sa) +
      geom_joy(
        aes(x = created_date,
            y = sentiment,
            fill = sentiment),
        rel_min_height = 0.01,
        alpha = 0.7,
        scale = 3
      ) +
      theme_joy() +
      labs(title = "Lecturers sentiment analysis",
           x = "Tweet Date",
           y = "Sentiment") +
      scale_fill_discrete(guide = FALSE)

    ## Picking joint bandwidth of 76

![](README_files/figure-markdown_strict/unnamed-chunk-31-1.png)

### Sentiment analysis full

    # By week
    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(sentiment)%>%
      ts_plot("weeks") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Sentiment analysis trend for all lecturers / Week")

![](README_files/figure-markdown_strict/unnamed-chunk-32-1.png)

    # By month
    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(sentiment)%>%
      ts_plot("months") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Sentiment analysis trend for all lecturers / month")

![](README_files/figure-markdown_strict/unnamed-chunk-32-2.png)

    # By year
    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      group_by(sentiment)%>%
      ts_plot("years") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Sentiment analysis trend for all lecturers / year")

![](README_files/figure-markdown_strict/unnamed-chunk-32-3.png)

### Sentiment analysis (positive / negative)

    # By week
    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      filter(sentiment =="positive" | sentiment =="negative")%>%
      group_by(sentiment)%>%
      ts_plot("weeks") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Positive / negative sentiment analysis trend for all lecturers  / Week")

![](README_files/figure-markdown_strict/unnamed-chunk-33-1.png)

    # By month
    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      filter(sentiment =="positive" | sentiment =="negative")%>%
      group_by(sentiment)%>%
      ts_plot("months") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Positive / negative sentiment analysis trend for all lecturers / month")

![](README_files/figure-markdown_strict/unnamed-chunk-33-2.png)

    # By year
    tweets_sa %>% 
      filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      filter(sentiment =="positive" | sentiment =="negative")%>%
      group_by(sentiment)%>%
      ts_plot("years") +
      theme_minimal() +
      labs(x = NULL, y = NULL,
           title = "Positive / negative sentiment analysis trend for all lecturers  / year")

![](README_files/figure-markdown_strict/unnamed-chunk-33-3.png)

### Comparison Word Cloud of positive and negative words

    tweets_sa %>% select(word) %>%
      inner_join(get_sentiments("bing")) %>%
      count(word, sentiment,sort = TRUE) %>%
      # spread(sentiment,n, fill = 0)%>% as.data.frame()
      reshape2::acast(word ~ sentiment, value.var = "n", fill = 0) %>%
      comparison.cloud(colors = c("red","green"),
                       max.words = 150)

    ## Joining, by = "word"

![](README_files/figure-markdown_strict/unnamed-chunk-34-1.png)

    saveRDS(tweets_sa,"data/tweets_sa.rds")
    rm(tweets_sa)

8 Spatial distribution
======================

    World <- map_data("world")
    tweets_geo <- tweets_tidy %>% select(name,lat,long)%>% 
      filter(!is.na(lat) & !is.na(long))

    # global
    ggplot() +
      geom_polygon(data = World,aes(x = long, y = lat, group = group), fill = "grey82", color = "white",alpha = 0.6)+
      theme_void() +
      coord_quickmap() +
      geom_point(data = tweets_geo,aes(x = long, y = lat),size = 2,alpha = 0.6) +
      labs(title = "Tweets world map") +
      theme_void() +
      theme(legend.position="bottom")

![](README_files/figure-markdown_strict/unnamed-chunk-35-1.png)

    # each lecturer
    ggplot() +
      geom_polygon(data = World,aes(x = long, y = lat, group = group), fill = "grey82", color = "grey82",alpha = 0.6)+
      theme_void() +
      coord_quickmap() +
      geom_point(data = tweets_geo,aes(x = long, y = lat),size = 1.5, color="blue") +
      facet_wrap(~name, ncol = 2)+
      labs(title = "Tweets world map\n") +
      theme_void() +
      theme(legend.position="none")

![](README_files/figure-markdown_strict/unnamed-chunk-35-2.png)

    rm(World,tweets_geo)

9 Social Network Analysis
=========================

    # Construct a document-feature matrix of Twitter posts
    tweets_full <- readRDS("data/tweets_full.rds")
    tweet_dfm <- tweets_full$text %>% 
      dfm(remove_punct = TRUE)

    rm(tweets_full)

9.1 show the most frequently used hashtags
------------------------------------------

    toptag <- dfm_select(tweet_dfm, pattern = ("#*")) %>%
      topfeatures(100) %>% names()

    to_remove <- c("#30daymapchallenge", "#albacete", "#argentina",
      "#auspol", "#coronavirus", "#covid19", "#egu18",
      "#egu19", "#elifesprint", "#epitwitter", "#esowc2019",
      "#eurocarto15", "#fail", "#fun", "#hobart", "#lbs2014",
      "#leeds", "#phd", "#qanda", "#rklive", "#runkeeper",
      "#runmeter", "#sil", "#tasmania", "#xijres")

    toptag <- toptag[- which(toptag %in% to_remove)]

    dfm_select(tweet_dfm, pattern = ("#*")) %>%fcm() %>%
      fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 10, edge_alpha = 0.8, edge_size = 5)

![](README_files/figure-markdown_strict/unnamed-chunk-37-1.png)

9.2 accounts from which most mentioned
--------------------------------------

    toptag <- dfm_select(tweet_dfm, pattern = ("@*")) %>%
      topfeatures(150) %>% names()

    to_remove <- c("@alankelloggs", "@eddierobson", "@christiancable", "@lfbarfe",
                   "@kisa", "@mjowen174", "@notrightaway", "@zevans23", "@ecmwf",
                   "@copernicusecmwf", "@copernicuseu", "@jeremy_morley","@archaeogeek",
                   "@kennethfield", "@stevenfeldman", "@ijturton", "@alexgleith")

    toptag <- toptag[- which(toptag %in% to_remove)]


    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>%
      fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 20, edge_color = "orange",edge_alpha = 0.8, edge_size = 5)

![](README_files/figure-markdown_strict/unnamed-chunk-38-1.png)
