#= Setup ======================
import GetOldTweets3 as got
import pandas as pd

#create a function to extract twitter information into a pandas df
def get_twitter_info():
    tweet_df["statuse_id"] = tweet_df["got_criteria"].apply(lambda x: x.id)
    tweet_df["link"] = tweet_df["got_criteria"].apply(lambda x: x.permalink)
    tweet_df["username"] = tweet_df["got_criteria"].apply(lambda x: x.username)
    tweet_df["to"] = tweet_df["got_criteria"].apply(lambda x: x.to)
    tweet_df["tweet_text"] = tweet_df["got_criteria"].apply(lambda x: x.text)
    tweet_df["date"] = tweet_df["got_criteria"].apply(lambda x: x.date)
    tweet_df["retweets"] = tweet_df["got_criteria"].apply(lambda x: x.retweets)
    tweet_df["favorites"] = tweet_df["got_criteria"].apply(lambda x: x.favorites)
    tweet_df["mentions"] = tweet_df["got_criteria"].apply(lambda x: x.mentions)
    tweet_df["hashtags"] = tweet_df["got_criteria"].apply(lambda x: x.hashtags)
    tweet_df["geo"] = tweet_df["got_criteria"].apply(lambda x: x.geo)

#= Search by username ==========
#set username and search variables
username = "pgrouting"
oldest_date = "2009-10-04"    
newest_date = "2009-10-19"
number_tweets = 4000

#get old tweets
tweet_criteria = got.manager.TweetCriteria().setUsername(username)\
                                            .setSince(oldest_date)\
                                            .setUntil(newest_date)\
                                            .setMaxTweets(number_tweets)
#extract twitter information
tweet_df = pd.DataFrame({'got_criteria':got.manager.TweetManager.getTweets(tweet_criteria)})
get_twitter_info()
tweet_df = tweet_df.drop("got_criteria", 1)
tweet_df.head()

#= Export df to csv =========================
tweet_df.to_csv("data/lecturers/"+username+".csv")
