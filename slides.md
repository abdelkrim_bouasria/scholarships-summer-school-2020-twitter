Introduction
------------

Google slides: [Twitter analysis](https://docs.google.com/presentation/d/e/2PACX-1vTPgoU2qxAPsDGY6w02Phib_RyJSvqO6K7YcvHvPQAOoPU-nDZSBYEfEJj0plbxMXjwVRNi96OdR1vK/pub?start=false&loop=false&delayms=3000)

Video presentation: [Twitter analysis](https://www.youtube.com/watch?v=fy1teuWC3Mo&list=PLXUoTpMa_9s0Ea--KTV1OEvgxg-AMEOGv&index=41)


Outline
-------

-   Collecting data from Twitter
    -   Twitter API - Method and tools
    -   Collected data first step - Old tweets
-   Data analysis
    -   Popular tweets - Tweeting activity analysis
    -   Most frequent words - Sentiment Analysis
    -   Spatial distribution - Social Network Analysis
-   Conclusion

I-Collecting data from Twitter
------------------------------

Twitter API
-----------

Method and tools
----------------

II-Exploratory data analysis
----------------------------

1 Collected tweets at 3.2k
--------------------------

    # 1. Tweets & Accounts statistics ====
    lecturers_metadata <- readRDS("data/lecturers_metadata.rds")
    tweets_3.2k <- readRDS("data/tweets_3.2k.rds")

    tweets_stat <- lecturers_metadata %>%
      mutate(statuses_at_3200 = ifelse(statuses_count > 3200, 3200, statuses_count)) %>% group_by(screen_name) %>%
      summarize(accounts = n(),tweets_count = sum(statuses_count),limit_3200 = sum(statuses_at_3200)) %>% 
      inner_join(tweets_3.2k %>% count(screen_name, name = "collected_at_3.2k"), by = "screen_name") %>%
      mutate(remaining = tweets_count - collected_at_3.2k)

    ## `summarise()` ungrouping output (override with `.groups` argument)

    tweets_stat_total <- colSums(tweets_stat[2:6])
    tweets_stat_total <- data.frame(
      screen_name = c("Total"),accounts = tweets_stat_total[[1]],
      tweets_count = tweets_stat_total[[2]], limit_3200 = tweets_stat_total[[3]],
      collected_at_3.2k = tweets_stat_total[[4]], remaining = tweets_stat_total[[5]])

    tweets_stat <- rbind(tweets_stat, tweets_stat_total)
    tail(tweets_stat)

    ## # A tibble: 6 x 6
    ##   screen_name    accounts tweets_count limit_3200 collected_at_3.2k remaining
    ##   <chr>             <dbl>        <dbl>      <dbl>             <dbl>     <dbl>
    ## 1 tom_hengl             1          446        446               446         0
    ## 2 underdarkGIS          1        14965       3200              3199     11766
    ## 3 VeronicaAndreo        1          937        937               932         5
    ## 4 volayaf               1          671        671               668         3
    ## 5 xurxosanz             1          283        283               179       104
    ## 6 Total                30       136703      38025             37762     98941

2 Missing Tweets
----------------

    tweets_date <- tweets_3.2k %>% 
      group_by(user_id) %>% summarize(first_tweet = min(created_at), last_tweet = max(created_at))

    ## `summarise()` ungrouping output (override with `.groups` argument)

    lecturers_metadata %>% 
      select(user_id,name,account_created_at) %>%
      mutate(name = str_remove(name, "#blacklivesmatter")) %>%
      mutate(name = ifelse(str_starts(name, "Barry Rowlingson"), "Barry Rowlingson", name)) %>%
      left_join(tweets_date, by = "user_id") %>%
      mutate(start_2 = first_tweet) %>%
      rename(start_1 = account_created_at,end_1 = first_tweet, end_2=last_tweet)%>%
      mutate_all(~replace(., is.na(.), ymd_hms("2012-07-30 10:52:58")))%>%
      mutate_at(c("start_1","end_1","end_2","start_2"),date)%>%
      gather(type,date,-user_id,-name) %>%
      separate(type, c("state","type")) %>%
      mutate(name = factor(name),type = factor(type, labels = c("missing","available")))%>%
      ggplot(aes(x=date,y=name, color = type, group=user_id)) +
      geom_line(size =2)+
      labs(x = "Date", y=NULL, title = "Lecturers: available / missing tweets ", 
           subtitle = "* get_timline() can retrieve tweets up to 3200 limit\n* lecturer Judith dont have tweets post ")+
      theme_bw()

![](slides_files/figure-markdown_strict/missins_tweets-1.png)

Collected old tweets (over time)
--------------------------------

    tweets_tidy%>%
      ggplot(aes(x = created_at, y = factor(name), color = tweet_age)) +
      geom_point(alpha = 0.6) +
      labs(title = "Tweets over time / lecturer", x = "Date", y = "Lecturers") +
      theme_bw() +
      theme(legend.position = "right")

![](slides_files/figure-markdown_strict/collected_old_tweets-1.png)

Collected old tweets (count by lecturer)
----------------------------------------

    tweets_tidy %>%  
      count(user_group,user_ord,screen_name,name,tweet_age)%>%
      ggplot(aes(x = reorder(name,desc(user_ord)), y = n, fill = tweet_age)) +
      geom_col() +
      labs(title = "Number of tweets / lecturer", x = "Lecturers", y = "Tweets count") +
      facet_wrap(~user_group, scales = "free_y")+
      coord_flip() +
      theme_minimal() +
      theme(legend.position = "bottom")

![](slides_files/figure-markdown_strict/tweets_type_lc-1.png)

III-Semantic data analysis
--------------------------

1 Popular tweets
----------------

    ## [1] TRUE TRUE TRUE

    ## [1] TRUE TRUE TRUE

-   *The most favorite tweet:* ![The most favorite
    tweet](screenshots/01fav.png)

-   *The most retweeted:* ![The most retweeted](screenshots/02retw.png)

-   *The most ranked tweet:* **favorite + retweeted** ![The most ranked
    tweet](screenshots/03rank.png)

2 Tweeting activity analysis
----------------------------

2.1 Temporal trends
-------------------

2.1.1 Overall trend
-------------------

    my_theme <- theme(axis.text.x = element_text(size = 10, face = "bold"),
          axis.text.y = element_text(size = 10, face = "bold"),  
          axis.title.x = element_text(size = 10, face = "bold"),
          axis.title.y = element_text(size = 10, face = "bold"),
          strip.text = element_text(size = 10, face="bold"),
          legend.text = element_text(size = 10, face="bold"),
          legend.title = element_text(size = 10, face="bold"))

    tweets_tidy %>% 
      ts_plot("months") +
      theme_minimal() +
      geom_line(size=1.2, color = "blue")+
      scale_x_datetime(date_breaks = "1 year", date_labels = "%Y")+
      ylim(-10,4100)+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / months [year-month]")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-3-1.png)

2.1.2 Overall trend and picks
-----------------------------

    library(ggpmisc)

    ## Warning: package 'ggpmisc' was built under R version 3.6.3

    tweets_tidy %>% 
      ts_plot("months") +
      theme_minimal() +
      geom_line(size=1.2, color = "blue")+
      scale_x_datetime(date_breaks = "1 year", date_labels = "%Y")+
       stat_peaks(colour = "red") +
      stat_peaks(geom = "text", colour = "red", hjust = -0.1, vjust = 0.1, 
                 angle = 45, check_overlap = TRUE, x.label.fmt = "%y-%m") +
      ylim(-10,4500)+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / months [year-month]")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-4-1.png)

2.1.3 Tweets type trend
-----------------------

    tweets_tidy %>% 
      group_by(tweet_type)%>%
      ts_plot("months") +
      theme_minimal() +
      geom_line(size=1.2)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      scale_color_brewer(palette = "Paired")+
      theme(legend.position = "bottom")+
      ylim(-10,2300)+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for all lecturers / month / type of tweets")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-5-1.png)

2.1.4 Lecturers group 1 trend
-----------------------------

    tweets_tidy %>% filter(user_group ==1) %>%
      group_by(name)%>%
      ts_plot("months") +
      theme_minimal() +
      geom_line(size=1.2)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      scale_color_brewer(palette = "Paired")+
      theme(legend.position = "bottom")+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / month [ group 1]")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-6-1.png)

2.1.5 Lecturers group 2 trend
-----------------------------

    tweets_tidy %>% filter(user_group ==2) %>%
      group_by(name)%>%
      ts_plot("months") +
      theme_minimal() +
      geom_line(size=1.2)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      scale_color_brewer(palette = "Paired")+
      theme(legend.position = "bottom")+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / month [ group 2]")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-7-1.png)

2.1.6 Lecturers group 3 trend
-----------------------------

    tweets_tidy %>% filter(user_group ==3) %>%
      group_by(name)%>%
      ts_plot("months") +
      theme_minimal() +
      geom_line(size=1.2)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      scale_color_brewer(palette = "Paired")+
      theme(legend.position = "bottom")+
      labs(x = NULL, y = NULL,
           title = "Tweeting frequency for each lecturer / month [ group 3]")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-8-1.png)

2.2 Tweets type Frequency
-------------------------

2.2.1 Tweets type overall frequency
-----------------------------------

    tweets_tidy %>% 
      count(tweet_type) %>%
      ggplot(aes(area=n, fill=tweet_type,label = tweet_type)) +
      geom_treemap() +
      geom_treemap_text(colour = "white", place = "centre")+
      theme_void() +
      scale_fill_brewer(palette="Set1")+
      theme(legend.position = "none")+
        labs( title = "Tweets type overall frequency\n")

![](slides_files/figure-markdown_strict/unnamed-chunk-9-1.png)

2.2.2 Tweets type frequency by lecturer
---------------------------------------

    tweets_tidy %>% 
      count(user_group,name,tweet_type) %>%
      ggplot(aes(area=n, fill=tweet_type)) +
      geom_treemap() +
      theme_void() +
      facet_wrap(user_group~name, ncol=5)+
      scale_fill_brewer(palette="Set1")+
      theme(legend.position = "bottom")+
        labs( title = "Tweets type frequency by lecturer") + 
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-10-1.png)

3 Most frequent words
---------------------

    most_frequent_50_words <- read_csv("frequent_words/most_frequent_50_words.csv")
    most_frequent_50_words %>% 
      arrange(desc(n)) %>% head(20)%>%
    ggplot(aes(x = reorder(word2,n), y = n)) +
      geom_col() +
      xlab(NULL) +
      coord_flip() +
      labs(y = "Count",
           x = "Unique words",
           title = "Count of 20 unique words found in all lecturers tweets",
           subtitle = "the retained list of 50 frequent keyword")

![](slides_files/figure-markdown_strict/most_50_words-1.png)

The most 5 influential lecturers
--------------------------------

    tweets_tidy_clean <- readRDS("data/tweets_tidy_clean_50_wodrs.rds")
    tweets_tidy_clean <- tweets_tidy_clean %>% count(word2, sort = TRUE) %>%
      top_n(6)  %>% inner_join(tweets_tidy_clean, by = "word2") %>%
      mutate(word2 = reorder(word2, n)) %>% select(3,5,8:11,1) %>% filter(word2!="rstats") 

    ## Selecting by n

    tweets_tidy_clean <- tweets_tidy_clean %>%
      inner_join(tweets_tidy_clean %>% count(name, sort = TRUE) %>% top_n(5), by="name")

    ## Selecting by n

    tweets_tidy_clean %>% distinct(name,n) %>% arrange(desc(n))

    ## # A tibble: 5 x 2
    ##   name               n
    ##   <chr>          <int>
    ## 1 Anita Graser    2975
    ## 2 Michael Sumner  1067
    ## 3 TimSalabim       739
    ## 4 Edzer Pebesma    683
    ## 5 Jakub Nowosad    545

Frequency of 5 hot keywords / 5 influential lecturers
-----------------------------------------------------

    tweets_tidy_clean %>% 
      count(name,word2) %>% rename(Keywords = word2)%>%
      ggplot(aes(area=n, fill=Keywords)) +
      geom_treemap() +
      theme_void() +
      scale_fill_brewer(palette="Set1")+
      facet_wrap(~name, ncol = 5)+ 
      theme(legend.position = "bottom")+
      labs( title = "Frequency of 5 hot keywords / 5 influential lecturers\n")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-12-1.png)

    tweets_tidy_clean %>% 
      count(name,word2) %>% rename(Lecturers = name) %>%
      ggplot(aes(area=n, fill=Lecturers)) +
      geom_treemap() +
      theme_void() +
      scale_fill_brewer(palette="Set1")+
      facet_wrap(~word2, ncol = 5)+ 
      theme(legend.position = "bottom")+
      labs( title = "Frequency of 5 influential lecturers / 5 hot keywords\n")+
      my_theme

![](slides_files/figure-markdown_strict/unnamed-chunk-13-1.png)

    # Overall
    tweets_tidy_clean %>%  ts_plot("months") + theme_minimal() +
      theme(legend.position = "bottom")+geom_line(size=1.2, color="blue")+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+my_theme+
      labs(x = NULL, y = NULL, title = "Trend of 5 keywords (posted by 5 lecturers) - by month")

![](slides_files/figure-markdown_strict/unnamed-chunk-14-1.png)

    # By Keyword
    tweets_tidy_clean %>% group_by(word2)%>%  ts_plot("months") + theme_minimal() + 
      theme(legend.position = "bottom")+geom_line(size=1.2)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      scale_color_brewer(palette = "Set1")+my_theme +
      labs(x = NULL, y = NULL, title = "Trend of 5 keywords (posted by 5 lecturers) - by month")

![](slides_files/figure-markdown_strict/unnamed-chunk-14-2.png)

    # By Lecturer
    tweets_tidy_clean %>% group_by(name)%>% ts_plot("months") + theme_minimal() + 
      theme(legend.position = "bottom") + geom_line(size=1)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      scale_color_brewer(palette = "Set1")+my_theme +
      labs(x = NULL, y = NULL, title = "Trend of 5 lecturers (5 keywords) - by month")

![](slides_files/figure-markdown_strict/unnamed-chunk-14-3.png)

4 Sentiment Analysis (SA)
-------------------------

4.1 Data preparation and cleaning and sentiment matching following NRC standard
-------------------------------------------------------------------------------

4.2 Overall SA frenquency
-------------------------

    tweets_sa %>% 
      count(sentiment) %>%
      ggplot(aes(area=n, fill=sentiment,label = sentiment)) +
      geom_treemap() +
      geom_treemap_text(colour = "white", place = "centre")+
      scale_fill_manual(values = c("#FF7F00", "#B2DF8A", "#1F78B4", "#FDBF6F", "#A6CEE3", "#E31A1C", "#33A02C", "#FB9A99", "#CAB2D6", "#6A3D9A"))+
      theme_void() +
      theme(legend.position = "none")+
      labs( title = " Frequency of sentiment in Tweets\n")

![](slides_files/figure-markdown_strict/unnamed-chunk-16-1.png)

### SA frenquency by lecturer

    tweets_sa %>% 
      count(name,sentiment) %>%
      ggplot(aes(area=n, fill=sentiment)) +
      geom_treemap() +
      theme_void() +
      facet_wrap(~name, ncol=5)+
      scale_fill_manual(name = "Sentiment:",values = c("#FF7F00", "#B2DF8A", "#1F78B4", "#FDBF6F", "#A6CEE3", "#E31A1C", "#33A02C", "#FB9A99", "#CAB2D6", "#6A3D9A"))+
      theme(legend.position = "bottom")+my_theme+
      labs( title = " Frequency of sentiment in Tweets by lecturer\n")

![](slides_files/figure-markdown_strict/unnamed-chunk-17-1.png)

### Frenquence ot sentiment type by tweet type

    tweets_sa %>% 
      count(tweet_type,sentiment) %>% ggplot(aes(area=n, fill=sentiment)) +
      geom_treemap() + theme_void() + facet_wrap(~tweet_type)+
      scale_fill_manual(name= "Sentiment:", values =c("#FF7F00", "#B2DF8A", "#1F78B4", "#FDBF6F", "#A6CEE3", "#E31A1C", "#33A02C", "#FB9A99", "#CAB2D6", "#6A3D9A"))+

      theme(legend.position = "right")+my_theme+
      labs( title = "Tweet type SA frequency\n")

![](slides_files/figure-markdown_strict/unnamed-chunk-18-1.png)

    tweets_sa %>% 
      filter(sentiment =="positive" | sentiment =="negative")%>%
      count(tweet_type,sentiment) %>%  ggplot(aes(area=n, fill=sentiment)) +
      geom_treemap() + theme_void() + facet_wrap(~tweet_type)+
      scale_fill_manual(name = "Sentiment:",values = c("#e31a1c","#33a02c"), labels=c("Negative","Positive"))+ 
      theme(legend.position = "right")+my_theme+
      labs( title = "Tweet type SA frequency (+ / -)\n")

![](slides_files/figure-markdown_strict/unnamed-chunk-18-2.png)

7.3 Temporal trend sentiment
----------------------------

### Overall trends

    tweets_sa %>% 
      group_by(sentiment)%>% ts_plot("months") +
      theme_minimal() +  geom_line(size=1.2)+
      #scale_color_brewer(name= "Sentiment:",palette = "Paired")+
      scale_color_manual(name= "Sentiment:", values =c("#FF7F00", "#B2DF8A", "#1F78B4", "#FDBF6F", "#A6CEE3", "#E31A1C", "#33A02C", "#FB9A99", "#CAB2D6", "#6A3D9A"))+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      theme(legend.position = "bottom")+my_theme+
      labs(x = NULL, y = NULL,
           title = "Lecturers sentiment analysis trend  / month")

![](slides_files/figure-markdown_strict/unnamed-chunk-19-1.png)

### Sentiment analysis (positive / negative)

    tweets_sa %>% 
      filter(sentiment =="positive" | sentiment =="negative")%>%
      group_by(sentiment)%>% ts_plot("months") +
      theme_minimal() + scale_color_manual(name= "Sentiment:",labels = c("Negative","Postive"),values = c("#e31a1c","#33a02c"))+ geom_line(size=1.2)+
      scale_x_datetime(date_breaks = "1 year",date_labels = "%Y")+
      theme(legend.position = "bottom")+my_theme+
      labs(x = NULL, y = NULL,
           title = "Positive / negative sentiment analysis trend for all lecturers / month")

![](slides_files/figure-markdown_strict/unnamed-chunk-20-1.png)

### Comparison Word Cloud of positive and negative words

    tweets_sa %>% select(word) %>%
      inner_join(get_sentiments("bing")) %>%
      count(word, sentiment,sort = TRUE) %>%
      reshape2::acast(word ~ sentiment, value.var = "n", fill = 0) %>%
      comparison.cloud(colors = c("#e31a1c","#33a02c"),
                       scale=c(8,.4),
                       max.words = 150)

![](slides_files/figure-markdown_strict/unnamed-chunk-21-1.png)

5 Spatial distribution
----------------------

    World <- map_data("world")
    tweets_geo <- tweets_tidy %>% select(name,lat,long)%>% filter(!is.na(lat) & !is.na(long))

    # global
    ggplot() +
      geom_polygon(data = World,aes(x=long, y=lat, group=group), fill="grey85", color="grey80")+
      theme_void() +coord_quickmap() +
      geom_point(data = tweets_geo,aes(x = long, y = lat),size = 2,alpha = 0.6) +
      labs(title = "Tweets world map") +theme_void() +theme(legend.position="bottom")

![](slides_files/figure-markdown_strict/unnamed-chunk-22-1.png)

    # By lecturer
    ggplot() +
      geom_polygon(data = World,aes(x=long,y=lat,group=group),fill="grey80",color="grey80")+
      theme_void() + coord_quickmap() +
      geom_point(data = tweets_geo,aes(x = long, y = lat),size = 1.5, color="blue") +
      facet_wrap(~name, ncol = 4)+ labs(title = "Tweets world map\n") +
      theme(legend.position="none") + theme(strip.text = element_text(size = 10, face="bold"))

![](slides_files/figure-markdown_strict/unnamed-chunk-22-2.png)

    rm(World,tweets_geo)

6 Social Network Analysis
-------------------------

    # Construct a document-feature matrix of Twitter posts
    tweets_full <- readRDS("data/tweets_full.rds")
    tweet_dfm <- tweets_full$text %>% dfm(remove_punct = TRUE)

    rm(tweets_full)

6.1 show the most frequently used hashtags
------------------------------------------

    library(readr)
    most_frequent_50_words <- read_csv("frequent_words/most_frequent_50_words.csv")

    ## Parsed with column specification:
    ## cols(
    ##   word2 = col_character(),
    ##   n = col_double()
    ## )

    toptag <- most_frequent_50_words$word2

    dfm_select(tweet_dfm, pattern = ("*")) %>%fcm() %>%
      fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 10, edge_alpha = 0.8, edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-25-1.png)

    most_frequent_100_clean_words <- read_csv("frequent_words/most_frequent_100_clean_words.csv")

    ## Parsed with column specification:
    ## cols(
    ##   word2 = col_character(),
    ##   n = col_double()
    ## )

    toptag <- most_frequent_100_clean_words$word2

    dfm_select(tweet_dfm, pattern = ("*")) %>%fcm() %>%
      fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 10, edge_alpha = 0.8, edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-27-1.png)

6.2 The most accounts from which most mentioned
-----------------------------------------------

    #= software/package, and organization tweets gathering =====

    soft_org_list <- c("qgis", "GRASSGIS", "gvsig", "GeoNode", "gvsig", "Landmark2020","opengeospatial", "osgeolive", "postgis", "opengeohub","open_EO", "BigDataEcology", "foss4g", "OSGeo", "planetosgeo", "Geo4All_", "OpenTopography", "mapserver_osgeo", "deegree_org", "openlayers","mapbender", "pywps", "GeoserverO", "pgrouting", "talk_rspatial")

    soft_org_metadata <- lookup_users(soft_org_list)
    lecturers_metadata <- lookup_users(soft_org_list)

    # = Friends tweets gathering ===========================================
    # 1. Friends ids gathering
    lecturers_soft_org <- rbind(lecturers_metadata,soft_org_metadata)
    friends <- as.data.frame(NULL)

    for(i in 1:nrow(lecturers_soft_org)){
      # first, check rate limits
      rate_limit_osgeos<-rate_limit()
      friends_ids<- rate_limit_osgeos[ rate_limit_osgeos$query=="friends/ids",]
      if(friends_ids$remaining < 2){
        Sys.sleep((friends_ids$reset + 1) *60)}
      rate_limit_status<- rate_limit_osgeos[ rate_limit_osgeos$query=="application/rate_limit_status",]
      if(rate_limit_status$remaining < 2){
        Sys.sleep((rate_limit_status$reset + 1)*60)}
      
      friends_step <- get_friends(lecturers_soft_org$screen_name[i],5000)
      friends <- rbind(friends, friends_step)
      Sys.sleep(1)
      # monitoring progress
      print(paste(i,lecturers_soft_org$screen_name[i],"=> friends_ids remaining:",friends_ids$remaining,
                  "rate_limit_status remaining:",rate_limit_status$remaining)) 
    }

    ## [1] "1 qgis => friends_ids remaining: 1 rate_limit_status remaining: 165"
    ## [1] "2 GRASSGIS => friends_ids remaining: 14 rate_limit_status remaining: 179"
    ## [1] "3 gvsig => friends_ids remaining: 13 rate_limit_status remaining: 178"
    ## [1] "4 GeoNode => friends_ids remaining: 12 rate_limit_status remaining: 177"
    ## [1] "5 Landmark2020 => friends_ids remaining: 11 rate_limit_status remaining: 176"
    ## [1] "6 opengeospatial => friends_ids remaining: 10 rate_limit_status remaining: 175"
    ## [1] "7 osgeolive => friends_ids remaining: 9 rate_limit_status remaining: 174"
    ## [1] "8 postgis => friends_ids remaining: 8 rate_limit_status remaining: 173"
    ## [1] "9 opengeohub => friends_ids remaining: 7 rate_limit_status remaining: 172"
    ## [1] "10 open_EO => friends_ids remaining: 6 rate_limit_status remaining: 171"
    ## [1] "11 BigDataEcology => friends_ids remaining: 5 rate_limit_status remaining: 170"
    ## [1] "12 foss4g => friends_ids remaining: 4 rate_limit_status remaining: 169"
    ## [1] "13 OSGeo => friends_ids remaining: 3 rate_limit_status remaining: 168"
    ## [1] "14 planetosgeo => friends_ids remaining: 2 rate_limit_status remaining: 167"
    ## [1] "15 Geo4All_ => friends_ids remaining: 1 rate_limit_status remaining: 166"
    ## [1] "16 OpenTopography => friends_ids remaining: 15 rate_limit_status remaining: 179"
    ## [1] "17 mapserver_osgeo => friends_ids remaining: 13 rate_limit_status remaining: 180"
    ## [1] "18 deegree_org => friends_ids remaining: 15 rate_limit_status remaining: 177"
    ## [1] "19 openlayers => friends_ids remaining: 12 rate_limit_status remaining: 180"
    ## [1] "20 mapbender => friends_ids remaining: 13 rate_limit_status remaining: 175"
    ## [1] "21 pywps => friends_ids remaining: 12 rate_limit_status remaining: 174"
    ## [1] "22 GeoserverO => friends_ids remaining: 11 rate_limit_status remaining: 173"
    ## [1] "23 pgrouting => friends_ids remaining: 12 rate_limit_status remaining: 180"
    ## [1] "24 talk_rspatial => friends_ids remaining: 12 rate_limit_status remaining: 179"
    ## [1] "25 qgis => friends_ids remaining: 11 rate_limit_status remaining: 178"
    ## [1] "26 GRASSGIS => friends_ids remaining: 11 rate_limit_status remaining: 177"
    ## [1] "27 gvsig => friends_ids remaining: 8 rate_limit_status remaining: 171"
    ## [1] "28 GeoNode => friends_ids remaining: 10 rate_limit_status remaining: 177"
    ## [1] "29 Landmark2020 => friends_ids remaining: 10 rate_limit_status remaining: 176"
    ## [1] "30 opengeospatial => friends_ids remaining: 9 rate_limit_status remaining: 175"
    ## [1] "31 osgeolive => friends_ids remaining: 9 rate_limit_status remaining: 174"
    ## [1] "32 postgis => friends_ids remaining: 5 rate_limit_status remaining: 169"
    ## [1] "33 opengeohub => friends_ids remaining: 4 rate_limit_status remaining: 168"
    ## [1] "34 open_EO => friends_ids remaining: 7 rate_limit_status remaining: 174"
    ## [1] "35 BigDataEcology => friends_ids remaining: 7 rate_limit_status remaining: 173"
    ## [1] "36 foss4g => friends_ids remaining: 7 rate_limit_status remaining: 173"
    ## [1] "37 OSGeo => friends_ids remaining: 2 rate_limit_status remaining: 165"
    ## [1] "38 planetosgeo => friends_ids remaining: 6 rate_limit_status remaining: 172"
    ## [1] "39 Geo4All_ => friends_ids remaining: 1 rate_limit_status remaining: 164"
    ## [1] "40 OpenTopography => friends_ids remaining: 14 rate_limit_status remaining: 179"
    ## [1] "41 mapserver_osgeo => friends_ids remaining: 13 rate_limit_status remaining: 178"
    ## [1] "42 deegree_org => friends_ids remaining: 12 rate_limit_status remaining: 177"
    ## [1] "43 openlayers => friends_ids remaining: 11 rate_limit_status remaining: 176"
    ## [1] "44 mapbender => friends_ids remaining: 10 rate_limit_status remaining: 175"
    ## [1] "45 pywps => friends_ids remaining: 9 rate_limit_status remaining: 174"
    ## [1] "46 GeoserverO => friends_ids remaining: 8 rate_limit_status remaining: 173"
    ## [1] "47 pgrouting => friends_ids remaining: 7 rate_limit_status remaining: 172"
    ## [1] "48 talk_rspatial => friends_ids remaining: 6 rate_limit_status remaining: 171"

    # Select friends following by 5 lecturers / closest at least

    friends_only <- friends %>% anti_join(lecturers_soft_org, by = "user_id" )
    friends_net <- friends_only %>% count(user_id) %>% filter(n > 5)
    friends_net <- distinct(friends_net, user_id)




    community_meta <- readRDS("data/community_meta.rds")
    toptag <- str_glue("@{name}",name=community_meta$screen_name)

At 5 of frequency
-----------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 5,  edge_color = "orange", edge_alpha = 0.8,omit_isolated = TRUE,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 3,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-29-1.png)

At 10 of frequency
------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 10,  edge_color = "orange", edge_alpha = 0.8,omit_isolated = TRUE,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 3,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-30-1.png)

At 15 of frequency
------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 15,  edge_color = "orange", edge_alpha = 0.8,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 4,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-31-1.png)

At 20 of frequency
------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 20,  edge_color = "orange", edge_alpha = 0.8,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 5,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-32-1.png)

At 25 of frequency
------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 25,  edge_color = "orange", edge_alpha = 0.8,omit_isolated = TRUE,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 5,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-33-1.png)

At 50 of frequency
------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 50,  edge_color = "orange", edge_alpha = 0.8,omit_isolated = TRUE,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 5,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-34-1.png)

At 100 of frequency
-------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>% fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 100,  edge_color = "orange", edge_alpha = 0.8,omit_isolated = TRUE,
                       vertex_color = "blue",vertex_labelcolor = "black",vertex_labelsize   = 5,edge_size = 5)

![](slides_files/figure-markdown_strict/unnamed-chunk-35-1.png)

At 200 of frequency
-------------------

    dfm_select(tweet_dfm, pattern = ("@*")) %>%fcm() %>%
      fcm_select(pattern = toptag)%>%
      textplot_network(min_freq = 200,
                       edge_color = "orange",
                       edge_alpha = 0.8,
                       vertex_size = 3,
                       vertex_labelsize=8,
                       edge_size = 2)

![](slides_files/figure-markdown_strict/unnamed-chunk-36-1.png)

7 Ratings & clustering
----------------------

Data preparation
----------------

    tweets_tidy <- readRDS("data/tweets_tidy.rds")
    tweets_tidy <- tweets_tidy %>% filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      select("tweet_type", "screen_name", "status_id", "favorite_count","retweet_count") %>%
      mutate_at(c(4:5), ~replace(., is.na(.), 0)) %>%
      mutate(rank_count = favorite_count+ retweet_count)

Lecturers order
---------------

    lecturers_ord <- tweets_tidy %>% count(screen_name, sort = TRUE) %>%
      mutate(user_ord = 1:30) %>% select(screen_name, user_ord)%>%
      mutate(user_group = c(rep(1, 10),rep(2,10), rep(3,10)))

    ratings_db <- lecturers_ord

    ratings_db %>% head()

    ## # A tibble: 6 x 3
    ##   screen_name     user_ord user_group
    ##   <chr>              <int>      <dbl>
    ## 1 geospacedman           1          1
    ## 2 mdsumner               2          1
    ## 3 underdarkGIS           3          1
    ## 4 chrisbrunsdon          4          1
    ## 5 TimSalabim3            5          1
    ## 6 precariobecario        6          1

Tweeting Activity score
-----------------------

    lecturers_ord <- tweets_tidy %>% count(tweet_type,screen_name) %>% 
      arrange(tweet_type,desc(n)) %>%
      mutate(tweet_activity = c(30:2,30:3,30:2,30:1)) %>% select(-n)%>%
      spread(tweet_type,tweet_activity, fill=0)

    ratings_db <- ratings_db %>% left_join(lecturers_ord)

    ratings_db %>% head()

    ## # A tibble: 6 x 7
    ##   screen_name     user_ord user_group organic quote reply retweet
    ##   <chr>              <int>      <dbl>   <dbl> <dbl> <dbl>   <dbl>
    ## 1 geospacedman           1          1      30    23    30      17
    ## 2 mdsumner               2          1      29    30    29      29
    ## 3 underdarkGIS           3          1      28    29    28      24
    ## 4 chrisbrunsdon          4          1      27    15    25      30
    ## 5 TimSalabim3            5          1      24    28    27      27
    ## 6 precariobecario        6          1      23    26    24      28

Tweeting Activity score by tweet type
-------------------------------------

    lecturers_ord <-   tweets_tidy %>% gather(reacting_type, reacting_count,-tweet_type,-screen_name,-status_id)%>%
      group_by(screen_name,reacting_type) %>% summarize(reacting_sum = sum(reacting_count))%>%
      spread(reacting_type,reacting_sum , fill=0)

    ratings_db <- ratings_db %>% left_join(lecturers_ord)

    ratings_db %>% select(screen_name,favorite_count:retweet_count) %>% head()

    ## # A tibble: 6 x 4
    ##   screen_name     favorite_count rank_count retweet_count
    ##   <chr>                    <dbl>      <dbl>         <dbl>
    ## 1 geospacedman             21286     531955        510669
    ## 2 mdsumner                 25027    3228786       3203759
    ## 3 underdarkGIS             52828     927719        874891
    ## 4 chrisbrunsdon             1640    5554833       5553193
    ## 5 TimSalabim3              11480     465134        453654
    ## 6 precariobecario          11115    2344350       2333235

Tweeting Activity score Total
-----------------------------

    lecturers_ord <- tweets_tidy %>% count(screen_name, sort=TRUE) %>% rename(tweet_count = n)
    ratings_db <- ratings_db %>% left_join(lecturers_ord)
    ratings_db <- ratings_db %>%select(1:7,11, everything())

    rm(tweets_tidy)

    ratings_db %>% select(retweet:retweet_count) %>%head()

    ## # A tibble: 6 x 5
    ##   retweet tweet_count favorite_count rank_count retweet_count
    ##     <dbl>       <int>          <dbl>      <dbl>         <dbl>
    ## 1      17       23219          21286     531955        510669
    ## 2      29       20586          25027    3228786       3203759
    ## 3      24        8851          52828     927719        874891
    ## 4      30        5013           1640    5554833       5553193
    ## 5      27        4030          11480     465134        453654
    ## 6      28        3482          11115    2344350       2333235

Most 5 frequent keywords
------------------------

    tweets_tidy_clean <- readRDS("data/tweets_tidy_clean_50_wodrs.rds")
    tweets_tidy_clean <- tweets_tidy_clean %>% count(word2, sort = TRUE) %>%
      top_n(6)  %>% inner_join(tweets_tidy_clean, by = "word2") %>%
      select(screen_name,word2) %>% filter(word2!="rstats") 

    ## Selecting by n

    lecturers_ord <- tweets_tidy_clean %>% count(screen_name,word2, sort = TRUE) %>% spread(word2,n, fill =0)
    ratings_db <- ratings_db %>% left_join(lecturers_ord)

    ## Joining, by = "screen_name"

    rm(tweets_tidy_clean )

    ratings_db %>% select(screen_name,foss4g:spatial)%>% head()

    ## # A tibble: 6 x 6
    ##   screen_name     foss4g  qgis raster rspatial spatial
    ##   <chr>            <dbl> <dbl>  <dbl>    <dbl>   <dbl>
    ## 1 geospacedman       151   254     37        2      95
    ## 2 mdsumner             4    88    628      102     245
    ## 3 underdarkGIS       160  2570     53        5     187
    ## 4 chrisbrunsdon        0     5      0        6      23
    ## 5 TimSalabim3         11    58    117      308     245
    ## 6 precariobecario      1     3      7       54      79

Sentiment Analysis
------------------

    tweets_sa <- readRDS("data/tweets_sa.rds")
    tweets_sa <-tweets_sa %>%filter(tweet_type!="quote_to" & tweet_type !="retweet_to")%>%
      select(screen_name,sentiment)

    lecturers_ord <-tweets_sa %>% count(screen_name,sentiment) %>% spread(sentiment,n, fill=0)
    ratings_db <- ratings_db %>% left_join(lecturers_ord)

    rm(tweets_sa, lecturers_ord)

    ratings_db <- ratings_db %>% mutate_at(c(2:26), ~replace(., is.na(.), 0))

    saveRDS(ratings_db,"data/ratings_db.rds")
    ratings_db %>% select(screen_name,anger:trust)%>% head()

    ## # A tibble: 6 x 11
    ##   screen_name anger anticipation disgust  fear   joy negative positive sadness
    ##   <chr>       <dbl>        <dbl>   <dbl> <dbl> <dbl>    <dbl>    <dbl>   <dbl>
    ## 1 geospacedm~  3440         7249    2438  4458  4978     8626    13458    4119
    ## 2 mdsumner     2759         6624    2442  3783  5655     7196    13846    3520
    ## 3 underdarkG~   489         3025     457  1014  2107     1647     6367     896
    ## 4 chrisbruns~  1148         2566     878  1932  1666     2637     4307    1256
    ## 5 TimSalabim3   368         1512     348   622  1198     1102     3555     533
    ## 6 precariobe~   244          646     257   383   437      604     1475     338
    ## # ... with 2 more variables: surprise <dbl>, trust <dbl>

Merge with metadata
-------------------

    ratings_db <- readRDS("data/ratings_db.rds")
    lecturers_metadata <- readRDS("data/lecturers_metadata.rds")
    lecturers_metadata <- lecturers_metadata %>% select(screen_name,name,followers_count,friends_count,statuses_count, favourites_count)

    ratings_db <- ratings_db %>% left_join(lecturers_metadata)

    ratings_db <- ratings_db %>% select(screen_name,name, everything())

    ratings_db %>% glimpse()

    ## Rows: 30
    ## Columns: 31
    ## $ screen_name      <chr> "geospacedman", "mdsumner", "underdarkGIS", "chris...
    ## $ name             <chr> "Barry Rowlingson\U0001f43a", "Michael Sumner", "A...
    ## $ user_ord         <dbl> 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,...
    ## $ user_group       <dbl> 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2,...
    ## $ organic          <dbl> 30, 29, 28, 27, 24, 23, 21, 26, 22, 25, 20, 13, 18...
    ## $ quote            <dbl> 23, 30, 29, 15, 28, 26, 24, 25, 20, 27, 21, 17, 18...
    ## $ reply            <dbl> 30, 29, 28, 25, 27, 24, 26, 22, 19, 16, 20, 18, 13...
    ## $ retweet          <dbl> 17, 29, 24, 30, 27, 28, 26, 23, 25, 21, 22, 20, 19...
    ## $ tweet_count      <dbl> 23219, 20586, 8851, 5013, 4030, 3482, 3453, 3010, ...
    ## $ favorite_count   <dbl> 21286, 25027, 52828, 1640, 11480, 11115, 13731, 20...
    ## $ rank_count       <dbl> 531955, 3228786, 927719, 5554833, 465134, 2344350,...
    ## $ retweet_count    <dbl> 510669, 3203759, 874891, 5553193, 453654, 2333235,...
    ## $ foss4g           <dbl> 151, 4, 160, 0, 11, 1, 19, 28, 9, 68, 9, 3, 178, 1...
    ## $ qgis             <dbl> 254, 88, 2570, 5, 58, 3, 29, 51, 2, 16, 17, 1, 71,...
    ## $ raster           <dbl> 37, 628, 53, 0, 117, 7, 100, 19, 1, 0, 50, 3, 8, 2...
    ## $ rspatial         <dbl> 2, 102, 5, 6, 308, 54, 256, 101, 14, 2, 264, 50, 4...
    ## $ spatial          <dbl> 95, 245, 187, 23, 245, 79, 279, 178, 33, 4, 205, 6...
    ## $ anger            <dbl> 3440, 2759, 489, 1148, 368, 244, 298, 320, 174, 12...
    ## $ anticipation     <dbl> 7249, 6624, 3025, 2566, 1512, 646, 1321, 1717, 879...
    ## $ disgust          <dbl> 2438, 2442, 457, 878, 348, 257, 263, 328, 132, 80,...
    ## $ fear             <dbl> 4458, 3783, 1014, 1932, 622, 383, 514, 562, 372, 2...
    ## $ joy              <dbl> 4978, 5655, 2107, 1666, 1198, 437, 953, 1138, 598,...
    ## $ negative         <dbl> 8626, 7196, 1647, 2637, 1102, 604, 873, 845, 557, ...
    ## $ positive         <dbl> 13458, 13846, 6367, 4307, 3555, 1475, 3040, 3609, ...
    ## $ sadness          <dbl> 4119, 3520, 896, 1256, 533, 338, 455, 488, 363, 12...
    ## $ surprise         <dbl> 3283, 3060, 1146, 929, 612, 222, 517, 668, 296, 29...
    ## $ trust            <dbl> 7937, 7583, 3121, 2601, 1783, 886, 1586, 1877, 110...
    ## $ followers_count  <int> 1612, 3001, 15746, 698, 2202, 1196, 4827, 4653, 61...
    ## $ friends_count    <int> 471, 4832, 1323, 708, 131, 1250, 481, 596, 781, 56...
    ## $ statuses_count   <int> 32110, 54970, 14965, 7368, 4579, 3678, 3408, 2847,...
    ## $ favourites_count <int> 1989, 24581, 7668, 6897, 6376, 5655, 6366, 6298, 2...

Ranting score
-------------

    ratings_db$score <-  round(rowSums(ratings_db[,5:26])/sum(ratings_db[,5:26]),digits = 2)*100
    score <- NULL
    score <- ratings_db %>% select(name,score) %>% arrange(desc(score))%>%
      mutate(name = str_remove(name, "#blacklivesmatter")) %>%
      mutate(name = ifelse(str_starts(name, "Barry Rowlingson"), "Barry Rowlingson", name))

    score_t <- NULL
    ratings_db$score_t <-  round(rowSums(ratings_db[,5:31])/sum(ratings_db[,5:31]),digits = 2)*100
    score_t <- ratings_db %>% select(name,score_t) %>% arrange(desc(score_t))%>%
      mutate(name = str_remove(name, "#blacklivesmatter")) %>%
      mutate(name = ifelse(str_starts(name, "Barry Rowlingson"), "Barry Rowlingson", name))

    scores <- cbind(score,score_t)
    names(scores)[3]<- "name_t"

    scores  %>% filter(score!=0 | score_t !=0)

    ##                    name score               name_t score_t
    ## 1        Chris Brunsdon    30       Chris Brunsdon      29
    ## 2        Michael Sumner    17       Michael Sumner      17
    ## 3  Virgilio Gómez-Rubio    12 Virgilio Gómez-Rubio      12
    ## 4          Roger Bivand     9         Roger Bivand       9
    ## 5          Anita Graser     5         Anita Graser       5
    ## 6           Daniel Nüst     5          Daniel Nüst       5
    ## 7     marta blangiardo      5    marta blangiardo        5
    ## 8      Barry Rowlingson     3     Barry Rowlingson       3
    ## 9            TimSalabim     3           TimSalabim       3
    ## 10        Theresa Smith     3        Theresa Smith       3
    ## 11         Paula Moraga     2         Paula Moraga       2
    ## 12        Edzer Pebesma     1        Edzer Pebesma       1
    ## 13       Robin Lovelace     1       Robin Lovelace       1
    ## 14      Verónica Andreo     1      Verónica Andreo       1

Clustering
----------

    ratings_db_sc <- ratings_db %>% select(1,5:26)
    ratings_db_m <- as.matrix(ratings_db_sc)
    rownames(ratings_db_m) <- ratings_db_sc$screen_name
    ratings_db_sc$screen_name <- NULL
    ratings_db_sc <- scale(ratings_db_sc)


    d <- dist(ratings_db_m, method = "euclidean")
    hc <- hclust(d, method = "complete" )

    par(cex=1, mar=c(2, 3, 2, 2))

    plot(hc, cex = 1, hang = -1,  xlab="", ylab="", main="")
    rect.hclust(hc,k = 6, border = 2:7)

![](slides_files/figure-markdown_strict/unnamed-chunk-46-1.png)

Conclusion
----------
